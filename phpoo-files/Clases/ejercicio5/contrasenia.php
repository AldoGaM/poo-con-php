<?php
    class Contrasenia {
        private $usuario;
        private $contra;

        public function __construct($usuario){
            $this->usuario = $usuario;
            $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $this->contra = substr(str_shuffle(($cadena)), 0, 4);
        }
        public function mostrar(){
            return 'Hola ' . $this->usuario . ', tu contraseña es ' . $this->contra;
        }
        public function __destruct(){
            echo 'La contraseña era ' . $this->contra . '<br />';
        }
    }
$mensaje2 = '';

if (!empty($_POST)){
    $contra1 = new Contrasenia($_POST["usuario"]);
    $mensaje2 = $contra1->mostrar();
}
?>