<?php
include_once('Carro4.php');
include_once('Avion.php');
include_once('Barco.php');
include_once('Cohete.php');

$mensaje='';


if (!empty($_POST)){
	//declaracion de un operador switch
	switch ($_POST['tipo_transporte']) {
		case 'aereo':
			//creacion del objeto con sus respectivos parametros para el constructor
			$jet1= new avion('jet','400','gasoleo','2');
			$mensaje=$jet1->resumenAvion();
			break;
		case 'terrestre':
			$carro1= new carro('carro','200','gasolina','4');
			$mensaje=$carro1->resumenCarro();
			break;
		case 'maritimo':
			$bergantin1= new barco('bergantin','40','na','15');
			$mensaje=$bergantin1->resumenBarco();
			break;
        case 'espacial';
            $cohete1 = new cohete('cohete', '28000', 'N y O2', 2);
            $mensaje = $cohete1->resumenCohete();
            break;
	}

}

?>