<?php
include_once('transporte.php');
// declaración de clase hijo
class cohete extends transporte {
    
    /* El número de fases de un cohete son las secciones en que están divididas
    las secciones de combustible y que se van soltando conforme asciende :) */
    private $numero_fases;

    // declaración de constructor
    public function __construct($nom, $vel, $com, $fas){
        parent::__construct($nom, $vel, $com);
        $this->numero_fases = $fas;
    }
    // declaración de método
    public function resumenCohete(){
        $mensaje = parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Número de fases:</td>
                    <td>' . $this->numero_fases . '</td>
                </tr>';
        return $mensaje;
    }
}
?>