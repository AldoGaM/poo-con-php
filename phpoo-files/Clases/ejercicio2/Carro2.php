<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $circula;
	//declaracion del método verificación
	public function verificacion($anio){
		if ($anio > 2010){
			$this->circula = "Sí";
		}elseif ($anio > 1989){
			$this->circula = "Revisión";
		}
		else{
			$this->circula = "No";
		}
	}
	public function getCircula(){
		return $this->circula;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$anio = substr($_POST['mes_fabricacion'], 0, 4);
	$Carro1->verificacion($anio);
}




